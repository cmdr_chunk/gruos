GRU DevLog 3 - Orion and dreams about GRU software future

This week I was trying to make interrupts, paging and other memory things working in Orion.
I found this guide and tried to follow it without rewriting the whole project:
[http://www.jamesmolloy.co.uk/index.html](http://www.jamesmolloy.co.uk/index.html)

For now only Global Descriptor Table maybe working.

My current goals/dreams: 

- make filesystem (the coolest would be make Ext2 driver)

- build GCC hosted compiler (this will help GCC understand our OS better)

My very future dreams:

- make kernel, libs, ports and soft in different repos

- make proper way to make "distros" on our kernel 

- make some website with mirror of ports scripts (like in Serenity OS but not in one repo with
everything and tool for searching and downloading ports from that mirror)

- maybe (if i will work a lot on this projects) I'll make one more account on tilde.team 
for our organisation (domain name - gru.ttm.sh, is cool), but i'll ask admins about that

Also I think Orion need new name because i found several projects with this name :)

Some ideas about the GRU name:

- GRU rocks (or rocking) UNIX

- GloRious UNIX 

- GloRious Union

If you can help me or have some ideas (name/names or anything else) contact me via email 
or any other type of contact (irc: g1n on tilde.chat, xmpp: g1n@hmm.st)

tags: gru, orion, dreams
