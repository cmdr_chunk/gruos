GRU Devlog 10 - orsh now like proper shell and gasm

Hooray! Today is 10's GRU Devlog! :)

This week i was trying to make notes to org document, to not forget something and put TODOs 
there for future weeks.

First several days was the most valuable - readline support and signal handling!
This means you can use emacs-like bindings there (but i think readline also supports vi bindings,
so maybe i will add them too!) 

Also we now have very minimal completion support there :), but still proper function need to be added 
for that (currently only filenames completion)

Orsh also has one session history, so you don't need to rewrite command from scratch now

And I have started to use it as my main (but had some problems after chsh, so currently is autostarts after bash
and i can exit from there any time i will need)

Then I have started making our own assembler - gasm. Currently it only supports NOP, so i haven't published it yet.

This week I also registered to ~news and shared devlog there (i think this devlog also will be there ;) )
If you have lobste.rs account please contact me to invite me, if you can, because it may advertize GRU more!

Today (Sunday) we had first testing GRU meeting via Jitsi. Thanks to r1k for joining.
We need to make something like plan our schedule (to know about what to speak) and discuss meeting time better.

Hope you liked this post and you will join our XMPP muc (if you are not already there) :). 
If you would like to help me, contact me via email, xmpp or irc :)

tags: gru, org-mode, gasm, orsh, meeting, lobste.rs, tilde.news
