GRU Devlog 21 - olibc

This week I also was making olibc. Now it is published! I have mostly implemented <string.h>, but there 
still some funcs that require <locale.h>, so they are not implemented yet.

I found out why olibc wasn't giving correct return value after program is executed. It was because
liblinux wasn't exiting correctly, so it received some random return values from stack.

Also a lot of testing were done. I have found out that several functions wasn't working correctly 
and fixed it. We have shell script for testing that compares results from our libc and libc that
installed on user's machine. It prints a diff output if test was failed. It also have other 
features, if you want you can check it by running `test.sh --help` by yourself.

Because of it I have found that several functions were wrong implemented. But now finally 
everything is working and strtok is correctly implemented!

Also this blog finally moved totally from my webpage, so we can move this site on different
server just by cloning it from codeberg or tildegit

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, olibc, liblinux
