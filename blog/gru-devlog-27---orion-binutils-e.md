GRU Devlog 27 - orion, binutils, E

Previous week I was working on orion. Implemented serial support and page frame allocator.
Also added some functions for serial for better debug output (warnings, todos, errors and other)
Page frame allocator and deallocator will be needed when making memory manager. I am going to
do that next week.

This week I started making binutils and E (from EGG). I am starting to write objdump. It 
already can disassemble some instructions, but for now it is very limited. Also yesterday
I found why framebuffer wasn't working and now E (maybe it will be "display server")
can write line on screen, but for some reason not more then one line.

I also had a lot of new ideas this week, but maybe they are just ideas. Compiled x86\_64 
gcc cross compiler, so maybe will be working on support of this arch soon. Also orion's
makefile is cleaner, so it maybe will be easier to port. I thought about making framebuffer
support using GRUB, but first I need to implement memory manager I think.

I feel like I forgot something that I was working on, but maybe they are things that are 
not relaleted to GRU (but could be useful in future, for example for testing)

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, orion, binutils, objdump, e, egg
