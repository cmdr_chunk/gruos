GRU Devlog 34 - txtutils, deertree

This 2 weeks I spent developing new programming language, called DeerTree. 
It is going to be combination of C, Rust and Go to make low-level development a bit simpler.

I have written some parts of specifications for language and standart library,
but I think it is not ready for publishing yet. 

But I have already started implementing compiler using lex and yacc. It already has lexer and
basic parser implemented, right now I am working on syntax tree.

Also I have published txtutils. It has just grep for now.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, txtutils, deertree
