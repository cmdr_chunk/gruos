GRU Devlog 7 - keyboard, paging and WIP initrd in Orion

This week I was improving Orion. First I made keyboard working, next day paging! 
Also I have added several new LibC functions. 
Today I have made scrolling working.

Last few days I was trying to make initrd working 
(this required heap implementing so I also did that but I don't know if it works correctly).
Currently it can only output /dev directory.

But I made something wrong with %x in printf and it can display weird chars first 
and then contents of files!

Hope you liked this post and if you would like to help me, contact me via email, xmpp or irc :)

tags: gru, orion
