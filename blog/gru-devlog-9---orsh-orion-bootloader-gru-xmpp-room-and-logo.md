GRU Devlog 9 - orsh, orion, bootloader, GRU xmpp room and logo

This week I didn't commited a lot, but started some new projects. 
I made orsh signal handling working (so it won't exit on ^C) and homedir "handling" 
(can replace homedir in prompt with ~, and you can use 'cd ~' or just 'cd' to change 
dir to homedir)

Also I started making bootloader, but for now it can only detect what CPU is (intel if x86 and amd if x86_64),
some additional instructions (msr) and if apic is avalible.

I was trying to make something with orion fs, maybe it will use pak files for initrd. Currently i am making archiver 
for it, but after that i will need to understand more how vfs working and rewrite initrd to use pak files.

We now have xmpp room (gru@conference.hmm.st) and logo! Thanks to chunk for it!

I was thinking about making weekly or monthly "conferences". We could do it via jitsi tildeverse instance or tilde.tel 
(tel.tilde.org.nz) conference. I think jitsi is better, but we should try tilde.tel confernce at least once, why not? :)

Hope you liked this post and you will join our XMPP muc :). If you would like to help me, contact me via email, xmpp or irc :)

tags: gru, orion, orsh, xmpp, tilde.tel, jitsi
