GRU Devlog 14 - ocpu and yemu 

This week I was working on ocpu emulator. So using it we can know if specifications can be implemented. 
It helps me adding some description to it.

First I made yemu a bit modular, so you need to add several lines to main file and then building your emulator
based on template (that should be placed in docs one day). Now if someone would like to add some architecture to 
our emulator they won't need to rewrite it from scratch.

I also added --system flag to yemu so architecture can be choosen more user-friendly way. 

Currently in ocpu emulator only mov and add instructions are implemented. 
We also added some description to commands in specifications, so it is easier to understand what it should do

Hope you liked this post! If you would like to help me, contact me via email, xmpp or irc :)

tags: gru, yemu, ocpu
