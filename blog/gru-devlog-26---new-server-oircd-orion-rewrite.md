GRU Devlog 26 - new server, oircd, orion rewrite

This 2 weeks were long for me. Previous week chunk donated domain and server for us so now this
blog and webpage is avalible on [https://gruos.org](https://gruos.org/). Also i setupped mail 
there and tryed to setup irc network, but we don't have services yet.

Also previous week I tryed to make hashmap and some other things for oIRCd, but it wasn't really success.

This week i started developing Orion rewrite. It already has interrupts working and enabled paging.
For now I don't know how to make timer and keyboard working. Currently I am trying to write page frame
allocator, it will open road to proper memory manager for us. I hope next few weeks I will focus on Orion.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, oircd, server, orion
