GRU Devlog 36 - coreutils

Last week I wasn't online for weekends, so decided to write devlog this week. 
Also, last 2 weeks I was mostly working on one of my admin projects 
([hextilde.xyz](https://hextilde.xyz)), so haven't done anything in that period of time.

This week I was working on coreutils. It wasn't maintained for a long time, so I decided to
add some utilities to it.

I have restructured it, rewrote cat and added some utilities - head and wc.

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, coreutils
