GRU Devlog 35 - deertree

This 2 weeks I was working on deertree specifications. They are
avalible on [https://gruos.org/deertree/](https://gruos.org/deertree), but weren't 
published on git because I think that they are not done yet.

Also I was working on compiler. First several days I was trying lex/yacc and even implemented
basic syntax tree. But then I changed to python (I named this compiler pydtc), because I don't 
fully understand syntax trees yet, so I hope in python it will be a bit easier. 

So far with python I made some progress and it can already output syntax tree for very basic code
and convert it to C or Assembler (yes, I added support for both).

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, deertree
