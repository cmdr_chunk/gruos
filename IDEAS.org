#+TITLE: Some dreams/ideas what to make

* Utils
** coreutils
   will be moved to coreutils/TODO
** binutils
- Assembler
- Dissambler
- Linker

** diffutils
** util-linux
** inetutils
** hexutils
- hexdump, xxd
* Libs
** libc
   - C11 libc implementation
   - libregex
   - other POSIX additions (unistd.h, termios.h and others)
  
** libcurses
** libgl
    
* Compilers
** OCC
  will be moved to occ/README.org
  
* Orion
  - Kernel separated from other software and is placed in one repo
  - Software repos separated per util or collection of utils (see Utils)
  - Ports is in `ports` repo and also can be found on `(MIRROR_OR_MAIN-WEBPAGE)/pub/ports`

* gxt (TUI Text Editor)
  - Have some shell (like M-x in emacs or : in vim)
    - Execute shell commands 
  - Scrolling using some keybindings
  - Have CLI mode
  - Configuring/Scripting/Plugins (using some language)
  - Be able to open more then one file (buffers
* yemu  
  - 6502 emulator
  - Own processor emulator :)
  - x86 emulator (in very far future :) )
* orsh
* Other
  - lex implementation
  - yacc implementation
  - m4
  - ed - standart editor
