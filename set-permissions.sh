#!/bin/bash
DIR=/var/www/html/gruos
USER1=chunk
USER2=www-data
cd $DIR
sudo chown -R $USER1:$USER2 *
sudo find $DIR -type f -exec chmod 664 {} \;
sudo find $DIR -type d -exec chmod 775 {} \;
sudo chmod +x set-permissions.sh
echo ""
echo "Success!"
echo ""
